FROM registry.access.redhat.com/ubi9/ubi:9.1

ADD certs/* /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust extract

RUN yum install -y git

CMD ["echo", "This is a 'Purpose-Built Container', It is only meant to be ran in a CI Pipeline. Please consult the documentation @ https://git.new"]
